import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {
  constructor(props){
    super(props);
    this.state={
      num:1,
      text:[],
    }
    this.addList=this.addList.bind(this)
  }
  render() {
    return (
      <div>
        <button className="add" onClick={this.addList}>Add</button>
        <div>
          <ul>
            {this.state.text.map((number) =>
            <li>{number}</li>)}
          </ul>
        </div>
      </div>

    );
  }

  addList(){
    this.setState({
      num:this.state.num+1
    });
    this.state.text.push("List Tittle"+this.state.num)

  }
}

export default TodoList;

