import React from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends React.Component{
  constructor(props){
    super(props);
    this.state={
      prompt:"Show",
      status:false
    }
    this.showOrHide=this.showOrHide.bind(this);
  }
  render() {
    return (
      <div className='App'>
        <button className="firstButton" onClick={this.showOrHide}>{this.state.prompt}</button>
        <button className="secondButton">refresh</button>
        {this.state.status==true?
        <TodoList/>
        :null}
      </div>
    )
  }
  showOrHide(){
    if (this.state.prompt=='Show'){
      this.setState({
        status:true
      })
      this.setState({
        prompt:"Hide"
      })
    }else{
      this.setState({
        status:false
      })
      this.setState({
        prompt:"Show"
      })
    }
  }
}

export default App;